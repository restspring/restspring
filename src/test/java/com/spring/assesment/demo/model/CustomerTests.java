package com.spring.assesment.demo.model;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class CustomerTests {
	private int id = 1;
	private String name="Bill";
	private String address = "55 Lane";

	private Customer testCustomer = new Customer(id,name,address);

	@BeforeClass
	public static void setUpBeforeClass() throws Exception { }

	@Test
	public void testCustomer() {
		assertNotNull(Customer.class);
	}

	@Test
	public void testCustomerBeingCreated() {
		Customer createCustomer = new Customer(id,name,address);
		assertNotNull(createCustomer);
	}

	@Test
	public void testGetId() {
		int expectedId=id;
		assertEquals(expectedId,testCustomer.getId());
	}

	@Test
	public void testSetId() {
		int originalId=id;
		int changedId = 2;

		assertEquals(originalId,testCustomer.getId());
		testCustomer.setId(changedId);
		assertEquals(changedId,testCustomer.getId());
	}

	@Test
	public void testGetName() {
		String expectedName=name;
		assertEquals(expectedName,testCustomer.getName());
	}

	@Test
	public void testSetName() {
		String originalName=name;
		String changedName = "Jill";

		assertEquals(originalName,testCustomer.getName());
		testCustomer.setName(changedName);
		assertEquals(changedName,testCustomer.getName());
	}

	@Test
	public void testGetAddress() {
		String expectedAddress=address;
		assertEquals(expectedAddress,testCustomer.getAddress());
	}

	@Test
	public void testSetAddress() {
		String originalAddress=address;
		String changedAddress = "123 Lane Road";

		assertEquals(originalAddress,testCustomer.getAddress());
		testCustomer.setAddress(changedAddress);
		assertEquals(changedAddress,testCustomer.getAddress());
	}

}
