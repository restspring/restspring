package com.spring.assesment.demo.rest;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.assesment.demo.model.Customer;
import com.spring.assesment.demo.repository.InMemoryCustomerDAO;

@RunWith(SpringRunner.class)
@WebMvcTest
public class CustomerControllerTests {

	private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private InMemoryCustomerDAO mockCustomerDao;

	@Test
	public void getAll_returnsList() throws Exception {
		when(mockCustomerDao.getAllCustomers()).thenReturn(new ArrayList<Customer>());

		MvcResult result = this.mockMvc.perform(get("/customer")).andExpect(status().isOk())
				.andExpect(jsonPath("$.size()").isNumber()).andReturn();

		logger.info("Result from customerDAO.getAllCustomers: " + result.getResponse().getContentAsString());
	}

	@Test
	public void save_returnsCreated() throws Exception {
		Customer customer1 = new Customer(1, "James", "22 circle street");

		this.mockMvc.perform(
				post("/customer").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(customer1)))
				.andExpect(status().isCreated()).andReturn();

		logger.info("Result from save customer");
	}

	@Test
	public void get_returnsOK() throws Exception {
		Customer customer1 = new Customer(1, "James", "22 circle street");

		when(mockCustomerDao.getCustomer(customer1.getId())).thenReturn(customer1);

		MvcResult result = this.mockMvc.perform(get("/customer/1")).andExpect(status().isOk()).andReturn();
		logger.info("Result from customerDao.getCustomer: " + result.getResponse().getContentAsString());
	}

	@Test
	public void delete_returnsOK() throws Exception {
		MvcResult result = this.mockMvc.perform(delete("/customer/1")).andExpect(status().isNoContent()).andReturn();

		logger.info("Result from customerDao.delete: " + result.getResponse().getContentAsString());
	}

}
