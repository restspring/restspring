package com.spring.assesment.demo.rest;

import java.awt.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.spring.assesment.demo.model.Customer;
import springfox.documentation.spring.web.json.Json;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class CustomerControllerIntegrationTests {

	private static Logger logger = LoggerFactory.getLogger(CustomerControllerIntegrationTests.class);

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void getCustomer_returnsCustomer() {
		restTemplate.postForEntity("/customer", new Customer(5, "harry", "40 round road"), Customer.class);

		ResponseEntity<Customer> getAllResponse = restTemplate.exchange("/customer/5", HttpMethod.GET, null,
				new ParameterizedTypeReference<Customer>() {
				});

		logger.info("getCustomer response: " + getAllResponse.getBody());

		assertEquals(HttpStatus.OK, getAllResponse.getStatusCode());
		assertEquals("harry", getAllResponse.getBody().getName());
		assertTrue(getAllResponse.getBody().getAddress().equalsIgnoreCase("40 round road"));
	}
	
	@Test
	public void getAll_returnsList() {
		restTemplate.postForEntity("/customer", new Customer(0, "harry", "40 round road"), Customer.class);
		ResponseEntity<ArrayList> getAllResponse = restTemplate.exchange(
				"/customer",
				HttpMethod.GET,
				null,
				new ParameterizedTypeReference<ArrayList>() {
				});

		logger.info("getAllCustomers response: " + getAllResponse.getBody());

		assertEquals(HttpStatus.OK, getAllResponse.getStatusCode());
		assertNotNull(getAllResponse.getBody());
	}
	
	
	
	
}