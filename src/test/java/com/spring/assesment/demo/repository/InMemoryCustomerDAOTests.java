package com.spring.assesment.demo.repository;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.spring.assesment.demo.exceptions.CustomerNotFoundException;
import org.junit.BeforeClass;
import org.junit.Test;

import com.spring.assesment.demo.model.Customer;

public class InMemoryCustomerDAOTests {

	private Customer customer1 = new Customer(1,"Matthew","40 Round Road");
	private Customer customer2 = new Customer(2,"James","40 Round Road");
	private InMemoryCustomerDAO  allCustomers =  new InMemoryCustomerDAO();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {}

	@Test
	public void testSaveCustomer() {
		allCustomers.saveCustomer(customer1);
		assert(allCustomers.getCustomer(customer1.getId()).equals(customer1));
	}

	@Test
	public void testGetCustomer() {
		allCustomers.saveCustomer(customer1);
		assert(allCustomers.getCustomer(customer1.getId()).equals(customer1));
	}

	@Test
	public void testGetAllProducts() {
		allCustomers.saveCustomer(customer1);
		allCustomers.saveCustomer(customer2);
		ArrayList list = (ArrayList) allCustomers.getAllCustomers();
	
		assert (list.get(0).equals(customer1));
		assert (list.get(1).equals(customer2));
	}

	@Test(expected = CustomerNotFoundException.class)
	public void testDeleteCustomerAndThrowErrorWhenCalled() {
		allCustomers.saveCustomer(customer1);
		allCustomers.deleteCustomer(customer1.getId());
		assert(allCustomers.getCustomer(customer1.getId())==null);
	}

}
