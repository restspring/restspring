package com.spring.assesment.demo.rest;

import com.spring.assesment.demo.exceptions.CustomerNotFoundException;
import com.spring.assesment.demo.model.Customer;
import com.spring.assesment.demo.repository.CustomerDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerDAO customerDAO;

    private static Logger LOG= LoggerFactory.getLogger(CustomerController.class);

    @RequestMapping(method = RequestMethod.GET)
    private List<Customer> getAll(){
        LOG.info("getAll was called");
        return customerDAO.getAllCustomers();
    }

    @RequestMapping(method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
    private HttpEntity<Customer> save(@RequestBody Customer customer){
        LOG.info("post was called");
        customerDAO.saveCustomer(customer);
        return new ResponseEntity<Customer>(customer, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    private Customer get(@PathVariable int id) {
       try {
            LOG.info("get was called");
            customerDAO.getCustomer(id);
        }catch (CustomerNotFoundException e){
           e.printStackTrace();
    }
        return customerDAO.getCustomer(id);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    private void delete(@PathVariable int id)  {
        LOG.info("delete was called");
        customerDAO.deleteCustomer(id);
    }
}
