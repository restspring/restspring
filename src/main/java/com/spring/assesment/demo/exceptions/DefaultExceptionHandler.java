package com.spring.assesment.demo.exceptions;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.annotation.Priority;

@ControllerAdvice
@Priority(1)
public class DefaultExceptionHandler {

    private static Logger LOG= LoggerFactory.getLogger(DefaultExceptionHandler.class);

    @ExceptionHandler(value = {CustomerNotFoundException.class})
    public ResponseEntity<Object> customerNotFoundExceptionHandler(CustomerNotFoundException e) {
        LOG.warn(e.toString());
        return new ResponseEntity<>("{\"message\": \"" + e.getMessage() + "\"}", HttpStatus.NOT_FOUND);
    }



}
