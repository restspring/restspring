package com.spring.assesment.demo.repository;

import com.spring.assesment.demo.model.Customer;

import java.util.List;

public interface CustomerDAO {

    void saveCustomer(Customer customer);

    Customer getCustomer(int id);

    List<Customer> getAllCustomers();

    void deleteCustomer(int id);


}
