package com.spring.assesment.demo.repository;

import com.spring.assesment.demo.exceptions.CustomerNotFoundException;
import com.spring.assesment.demo.model.Customer;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class InMemoryCustomerDAO implements CustomerDAO {

    private Map<Integer, Customer> allCustomers = new HashMap<Integer, Customer>();

    @Override
    public void saveCustomer(Customer customer) {
        allCustomers.put(customer.getId(), customer);
    }

    @Override
    public Customer getCustomer(int id) {
        if(allCustomers.get(id)==null){
            throw new CustomerNotFoundException("No Customer of ID "+id+" was found.");
        }
        return allCustomers.get(id);
    }

    @Override
    public List<Customer> getAllCustomers() {
        return new ArrayList<Customer>(allCustomers.values());
    }

    @Override
    public void deleteCustomer(int id) {
        if(allCustomers.remove(id)==null){
            throw new CustomerNotFoundException("No Customer of ID "+id+" was found.");
        }
    }

}
