package com.spring.assesment.demo.app;

import com.spring.assesment.demo.model.Customer;
import com.spring.assesment.demo.repository.CustomerDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class Application implements ApplicationRunner {

    @Autowired
    private CustomerDAO customerDAO;

    public void run(ApplicationArguments appArgs) {
        System.out.println("Creating some customers");
        String[] customerNames = {"Bill", "Jane", "Frank"};
        String[] customerAddresses = {"22 Hill Lane", "54 Main Street", "64 Zoo Lane"};

        for (int i = 0; i < customerNames.length; ++i) {
            Customer thisCustomer = new Customer(i,
                    customerNames[i],
                    customerAddresses[i]);

            System.out.println("Created Product: " + thisCustomer);

            customerDAO.saveCustomer(thisCustomer);
        }

        System.out.println("\nAll Customers:");
        for (Customer customer : customerDAO.getAllCustomers()) {
            System.out.println(customer);
        }

        System.out.println("Demo Finished - Exiting");
    }
}
